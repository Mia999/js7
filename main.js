function calculateGeneralizedFibonacci(F0, F1, n) {
    if (n === 0) {
        return F0;
    } else if (n === 1) {
        return F1;
    }

    let current = 0;
    let prev1 = F1;
    let prev2 = F0;

    for (let i = 2; i <= Math.abs(n); i++) {
        current = prev1 + prev2;
        prev2 = prev1;
        prev1 = current;
    }

    if (n < 0 && n % 2 === 0) {
        current = -current;
    }

    return current;
    
}

const n = parseInt(prompt("Введите порядковый номер n:"));



// Ввод значения F0 и F1
const F0 = parseInt(prompt("Введите значение F0:"));
const F1 = parseInt(prompt("Введите значение F1:"));



// Вызываем функцию и выводим результат
const result = calculateGeneralizedFibonacci(F0, F1, n);
console.log(`n-е обобщенное число Фибоначчи: ${result}`);
